import time


class Cat:
    time_slept = 0

    def sleep(self):
        sleep_time = 5
        time.sleep(sleep_time)
        self.time_slept += sleep_time
        print(f"Slept for {self.time_slept} seconds in total")

    def hear_and_ignore(self):
        command = input("Enter your command to the cat: ")
        print("Successfully ignored command: " + command)
