import pytest
import pytest_mock
import sys
sys.path.append("..")
from src.cat import Cat

def test_sleep(mocker):
    mocker.patch("time.sleep")
    cat_under_test = Cat()
    cat_under_test.sleep()
    assert cat_under_test.time_slept == 5


def test_hear_and_ignore(mocker, capsys):
    mocker.patch("builtins.input", return_value="Get down!")
    cat_under_test = Cat()
    cat_under_test.hear_and_ignore()
    captured = capsys.readouterr()
    assert captured.out == "Successfully ignored command: Get down!\n"
